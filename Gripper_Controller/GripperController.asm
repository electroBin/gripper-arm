;Control a robotic gripper arm

    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>	; processor specific variable definitions
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -312     ;no  "page or bank selection not needed" messages
    errorlevel -207    ;no label after column one warning
	
    #define BANK0	       (h'000')
    #define BANK1	       (h'080')
    #define BANK2	       (h'100')
    #define BANK3	       (h'180')
    #define gripperWrite  (b'00010000')	    ;Bus address for i2c gripper controller write (d'16')
    #define screwStop	       (d'94')	    ;PWM stop signal for lead screw
    

    __CONFIG _CONFIG1,    _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC_INTOSC & _FCMEN_OFF & _IESO_OFF

;Context saving variables:
MULTIBANK	    UDATA_SHR
w_copy		    RES 1	;variable used for context saving (work reg)
status_copy	    RES 1	;variable used for context saving (status reg)
pclath_copy	    RES 1	;variable used for context saving (pclath copy
userMillis	    RES	1
dly16Ctr	    RES	1
initCounter	    RES	1	    
		    
;General Variables
GENVAR		    UDATA
i2cByteToSend	    RES	1	;Data to be sent from slave		
leadScrewPWM	    RES	1	;PWM value to be sent to lead screw motor
gripTiltPWM	    RES	1	;PWM value to be sent to gripper tilt servo
gripClawPWM	    RES	1	;PWM value to be sent to gripper claw servo	
gripRdyFlag	    RES	1	;Flag to signify that all 3 PWM values for
				;gripper arm control have been received
command		    RES	1	;12c command from master device	  
SSPBUFC		    RES	1	;Copy of SSPBUF		    
gripPacketCtr	    RES	1	;Keeps track of i2c packets for robo arm	    

;**********************************************************************
    ORG		0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    banksel	w_copy
    movwf       w_copy           ;save off current W register contents
    movfw        STATUS         ;move status register into W register
    movwf       status_copy      ;save off contents of STATUS register
    movf        PCLATH,W
    movwf       pclath_copy
    
    ;Determine source of interrupt (PORTB change or I2C)
    ;Priority given to I2C interuppts so check it 1st.
    banksel	PIR1
    btfss	PIR1, SSPIF
    ;btfsc	INTCON, IOCIF
    goto	HallEffect
    
    ;Determine if STOP bit triggered interrupt
    banksel	SSPSTAT
    btfsc	SSPSTAT, P
    goto	GripperCommDone
    
    ;For I2C interrupt, clear PIR1, SSPIF flag. A bus collision also causes
    ;an interrupt so clear PIR2, BCLIF also. After interrupt is triggered upon 
    ;address match, SSPSTAT, BF (Buffer full status) must be cleared by reading
    ;SSPBUF
  
    ;Check for collisions and overflows
    banksel	PIR2
    bcf		PIR2, BCLIF
    banksel	SSPCON1
    btfsc	SSPCON1, SSPOV
    bcf		SSPCON1, SSPOV
    btfsc	SSPCON1, WCOL
    bcf		SSPCON1, WCOL
    
    ;clrf	INTCON
    ;banksel	PIE1
    ;bcf		PIE1, SSPIE	;Disable serial port interrupts while we do this
	;ACK pulse sent by slave on the 9th bit. (hardware)
	;After ACK, slave hardware clears SSPCON1, CKP and SCL pin is held low 
	;(Master can't send anything during this time while slave prepares data to be transmitted)
    ;Read the received address from SSPBUF to clear SSPSTAT, BF flag
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear interrupt flag
    banksel	SSPBUF
    movfw	SSPBUF
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line (we are stretcing clock)

    banksel	PIR1
waitPWM
    btfss	PIR1, SSPIF	    ;Wait for buffer to fill
    goto	waitPWM
    
    banksel	SSPBUF
    movfw	SSPBUF
    banksel	SSPBUFC		    ;Read buffer and make a copy
    movwf	SSPBUFC
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear interrupt flag
    ;Is this a command or a PWM value being sent from master? (commands <= 4)
    movlw	.5
    banksel	SSPBUFC
    subwf	SSPBUFC, w
    btfsc	STATUS, C	    ;C=0 if neg result
    goto	GetPWM
    
GetCommand
    banksel	SSPBUFC
    movfw	SSPBUFC
    movwf	command		    ;Store command
    goto	GripperCommDone
    
GetPWM    
    ;What was the last command?
    banksel	command
    btfsc	command, 0	   ;b'00000001' = lead screw ESC
    goto	Screw
    btfsc	command, 1	   ;b'00000010' = gripper tilt
    goto	Tilt
    btfsc	command, 2	   ;b'00000100' = gripper jaw
    goto	Jaw
    goto	GripperCommDone
Screw
    ;Make sure value is within acceptable range (68-118)
    movlw	.68
    banksel	SSPBUFC
    subwf	SSPBUFC, w
    btfss	STATUS, C
    goto	GripperCommDone	    ;Value too low
    
    movlw	.119
    banksel	SSPBUFC
    subwf	SSPBUFC, w
    btfsc	STATUS, C
    goto	GripperCommDone	    ;Value too high
    
    ;Determine if forward or reverse signal is being sent:
    movlw	.94	;94=stopped
    banksel	SSPBUFC
    subwf	SSPBUFC, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	screwFwd
    
screwRev
    ;Is PORTB, 1 low (low=screw is already max-retracted)
    banksel	PORTB
    btfss	PORTB, 1
    goto	MaxRetracted	    ;Screw is already in max-retracted position
    banksel	SSPBUFC
    movfw	SSPBUFC
    banksel	CCPR2L
    movwf	CCPR2L
    ;bsf		INTCON, IOCIE	    ;Reenable IOC for PORTB
    goto	GripperCommDone
MaxRetracted    
    movlw	screwStop
    banksel	CCPR2L
    movwf	CCPR2L
    goto	GripperCommDone
    
screwFwd
    ;Is PORTB, 0 low (low=screw is already max-extended))
    banksel	PORTB
    btfss	PORTB, 0
    goto	MaxExtended	    ;Screw is already in max-extended position
    banksel	SSPBUFC
    movfw	SSPBUFC
    banksel	CCPR2L
    movwf	CCPR2L
    ;bsf		INTCON, IOCIE	    ;Reenable IOC for PORTB
    goto	GripperCommDone
MaxExtended    
    movlw	screwStop
    banksel	CCPR2L
    movwf	CCPR2L
    goto	GripperCommDone

Tilt
    banksel	SSPBUFC
    movfw	SSPBUFC
    banksel	CCPR1L
    movwf	CCPR1L
    goto	GripperCommDone
Jaw   
    banksel	SSPBUFC
    movfw	SSPBUFC
    banksel	CCPR3L
    movwf	CCPR3L
    goto	GripperCommDone
    
GripperCommDone 
    ;Clear any lingering flags
    banksel	PIR1
    bcf		PIR1, SSPIF
    banksel	PIR2
    bcf		PIR2, BCLIF
    banksel	PIE1
    bsf		PIE1, SSPIE	;Re-enable serial port interrupts
    movlw	b'11000000'
    movwf	INTCON
    banksel	SSPCON1
    bsf		SSPCON1, CKP	    ;Release clock line (we are stretcing clock)
    goto	InterruptDone
;Interrupt is triggered by PortB, 0/1 going low     
HallEffect
    banksel	IOCBF
    clrf	IOCBF
    movlw	screwStop
    banksel	CCPR2L
    movwf	CCPR2L
    ;disable IOC for PORTB for now (valid PWM value will reenable it)
    ;bcf		INTCON, IOCIE
    
InterruptDone    
    banksel	pclath_copy
    movfw	pclath_copy
    movwf	PCLATH
    movf	status_copy,w   ;retrieve copy of STATUS register
    movwf	STATUS          ;restore pre-isr STATUS register contents
    swapf	w_copy,f
    swapf	w_copy,w
    
    retfie
    
;***************************I2C subroutines*************************************
sendI2Cbyte
    banksel	i2cByteToSend
    movfw	i2cByteToSend
    banksel	SSPBUF
    movwf	SSPBUF		;byte is already in work register
    ;banksel	SSPSTAT
    ;btfsc	SSPSTAT, BF	;wait till buffer is empty (when BF=0, transmit complete)
    ;goto	$-1		;not full, wait here
   
    retlw	0
waitNack
    banksel	SSPCON2
    btfss	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0
waitACK
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0
;**************************END i2c ROUTINES*************************************   
    
;******Initialize ESCs with stoped signal (1500uS) for four seconds*************
ESCinit
    movlw	.25
    movwf	initCounter	;16 calls to delayMillis at 250ms each = 4 sec
    movlw	.95		;1500uS pulse width
    banksel	CCPR2L		;ESC #2
    movwf	CCPR2L
beginInit
    movlw	.250
    pagesel	delayMillis
    call	delayMillis
    pagesel$
    decf	initCounter, f
    movlw	.0
    xorwf	initCounter, w
    btfss	STATUS, Z
    goto	beginInit
    
    retlw	0    
    
;******************Variable millisecond delay routuine**************************
delayMillis
    banksel	userMillis
    movwf	userMillis	    ;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    banksel	TMR0
    movfw	TMR0
    xorlw	.125	    ;125 * 8uS = 1mS
    btfss	STATUS, Z
    goto	waitTmr0
    banksel	userMillis
    decfsz	userMillis, f   ;reached user defined milliseconds yet?
    goto        startDly
    retlw	0

start:
    
    movlw	b'01101000'
		 ;-1101---	;Internal oscillator = 4 MHz
    banksel	OSCCON
    movwf	OSCCON
    
    banksel	BANK1
    ;Set PORTS to output
    movlw	b'11111111'		
    movwf	(TRISA ^ BANK1)
    movlw	b'11111111'		    
    movwf	(TRISB ^ BANK1)
    movlw	b'11111001'	
		 ;1-------	UART RX
		 ;-1------	UART TX
		 ;----1---      I2C SCL pin
		 ;---1----	I2C SDA pin
		 ;-----0--	Gripper Tilt PWM
		 ;------0-	Lead Screw PWM
    movwf	(TRISC ^ BANK1)
    movlw	b'00000000'
    movwf	(TRISD ^ BANK1)	    
    movlw	b'00001110'
		 ;-------0	;Gripper claw PWM
    movwf	(TRISE ^ BANK1)
    ;************************Configure timer0************************************
    
    movlw	b'10000100'	
		 ;1-------	WPUEN=0, all weak pull-ups are disabled
		 ;-0------	INTEDG=0, No interrupt on rising edge of INT pin
		 ;--0-----	TMR0CS=0, TMR0 clock source=internal instruction
			        ;	  (FOSC/4)
		 ;---0----	;TMR0SE=0, TMR0 increments on low to high 
				;transotion of RA4/T0CKI pin
		 ;----0---	;PSA=0, prescaler assigned to TMR0 module
		 ;-----100	;PS<2:0> = 100, TMRO increments once every 32
				;instruction cycles 
				;Every instruction cycle is 1uS sec (1Mhz), 
				;therefore TMR0 increments once every 32 uS
				
    banksel	OPTION_REG	
    movwf	OPTION_REG
    
    movlw	b'01101000'     ;Internal oscillator set to 4MHz
    banksel	OSCCON
    movwf	OSCCON

    ;*********************Configure I2C*****************************************
    movlw	b'00010000'
		 ;-------0	;Bit 0 not used in 7-bit slave mode
		 ;00010000-	;7-bit address for i2c slave mode.  Address for
				;gripper controller will be d'8'. The 1st bye received
				;after a Start or Restart is compared to the value 
				;in this register. An interrupt is generated if match
				;occurs
    banksel	SSPADD
    movwf	SSPADD
    
    movlw	b'11111110'
		 ;-------0	;Bit 0 is not used in 7-bit slave mode
		 ;11111110-	;Mask for 7-bit i2c address sent from master. Used
				;to detect address match.
    banksel	SSPMSK
    movwf	SSPMSK
    
    movlw	b'10000000'
		 ;1-------	'SMP=1, data sampled at end of data output time
				;slew rate control in 100kHz mode
    banksel	SSPSTAT
    movwf	SSPSTAT
    
    movlw	b'00111110'
		 ;-0------	;SSPOV=receive overflow indicator (read data in 
				;sspbuf before new data comes in to prevent error.
				;Check and clear to ensure sspbuf can be updated
		 ;--1-----	;SSPEN=1 (Enable I2C serial port)
		 ;---1----	;Enable Clock (CKP=1)
		 ;----1110	;I2C slave mode, 7-bit address with Start and
				;Stop bit interrupts enabled
    banksel	SSPCON1
    movwf	SSPCON1
    
    banksel	SSPCON2
    bsf		SSPCON2, SEN	;Enable clock stretching
    
    movlw	.10
    pagesel	delayMillis
    call	delayMillis
    pagesel$

;**********************Configure Lead Screw ESC PWM*****************************
    movlw	b'00000110'     ; configure Timer2:
		; -----1--          turn Timer2 on (TMR2ON = 1)
		; ------11          prescale = 64 (T2CKPS = 11)
    banksel	T2CON           ; -> TMR2 increments every 64 us
    movwf	T2CON
    
    movlw	.250             ; PR2 = 178
    banksel	PR2             ; -> period = 2.86mS
    movwf	PR2             ; -> PWM frequency = 350 Hz
    ;Configure CCP2  to be based off of TMR2:
    banksel	CCPTMRS0
    movlw	b'11110011'
		 ;----00--	;CCP2 based off of TMR2 (P2A=lead screw)
    movwf	CCPTMRS0
    
    ;configure CCP1, CCP2, CCP3 and CCP5:
    movlw	b'00001100'     
		; 00------          single output (P1M = 00 -> CCP1 active)
		; --00----          DC1B = 00 -> LSBs of PWM duty cycle = 00
		; ----1100          PWM mode: all active-high (CCP1M = 1100)
    banksel	CCP1CON         ; -> single output (CCP1) mode, active-high
    movwf	CCP1CON
    banksel	CCP2CON
    movwf	CCP2CON
    banksel	CCP3CON
    movwf	CCP3CON
    banksel	CCP5CON
    movwf	CCP5CON
    
    movlw	screwStop	;1500uS pulse width (default = stopped)
    banksel	CCPR2L		;Lead screw ESC
    movwf	CCPR2L
    
    
;***************Configure Gripper Servo Tilt and Jaws  PWM**********************
    movlw   b'00000110'
	     ;-----1--	enable TMR4
	     ;------10	prescale=16 (T4CKPS=11)
    banksel T4CON	    ; -> TMR4 increments every 16 us
    movwf   T4CON
    
    movlw   .255
    banksel PR4	    ; -> period = 256*16uS=4.1 mS
    movwf   PR4	    ;Frequency = 244 Hz
    ;Configure CCP1 and CCP3 to be based off of TMR4:
    banksel CCPTMRS0
    movlw   b'00010001'	
	     ;--01----	    ;CCP3 is based on TMR4
	     ;------01	    ;CCP1 is based on TMR4
    movwf   CCPTMRS0	    ;bits 0:1=1=TMR4 for CCP5
    
    movlw   .94
    banksel CCPR1L
    movwf   CCPR1L
    
    movlw   .94
    banksel CCPR3L
    movwf   CCPR3L

    ;***********************Disable Comparators*********************************
    banksel	CM1CON0
    bcf		CM1CON0, 7
    banksel	CM2CON0
    bcf		CM2CON0, 7
    
    banksel	ANSELA
    clrf	ANSELA
    clrf	ANSELB
    clrf	ANSELD
    clrf	ANSELE
    
;******************Enable Interrupts********************************************
    movlw	b'11001000'
	         ;1-------	;Enable global interrupts (GIE=1)
		 ;-1------	;Enable peripheral interrupts (PEIE=1)
		 ;--0-----	;Disable TMR0 interrupts (TMROIE=0)
		 ;---0----	;Disable RBO/INT external interrupt (INTE=0)
		 ;----1---	;Enable interrupt on change for PORTB (IOCIE=1)
    movwf	INTCON
    
    banksel	PIR1
    bcf		PIR1, SSPIF	;Clear MSSP interrupt flag
    
    movlw	b'00001000'
		 ;----1---	;Synchronous Serial Port (SSP) interrupts enabled.
				;Used for i2c interrupts
    banksel	PIE1
    movwf	PIE1
    
    ;Enable IOC for PORTB, 0 (extended hall effect sensor) and PORTB, 1 (retracted
    ;hall-effect sensor). Interrupt triggered on falling edge
    movlw	b'00000011'
    banksel	IOCBN
    movwf	IOCBN
    
    banksel	PORTB
    clrf	PORTB
    clrf	PORTC
    clrf	PORTD
    clrf	userMillis
    movlw	.23
    banksel	leadScrewPWM
    movwf	leadScrewPWM
    movwf	gripTiltPWM
    movwf	gripClawPWM
    clrf	command
    clrf	SSPBUFC
    clrf	gripPacketCtr
    
    call	ESCinit
    
mainLoop

    goto	mainLoop
   
    END                       


























