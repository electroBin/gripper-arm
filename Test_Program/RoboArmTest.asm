;Test the communication between an external device and the battery monitor
;circuit   

    list	p=16f1937	;list directive to define processor
    #include	<p16f1937.inc>		; processor specific variable definitions
	
    errorlevel -302	;no "register not in bank 0" warnings
    errorlevel -312     ;no  "page or bank selection not needed" messages
    errorlevel -207    ;no label after column one warning
    
    #define gripperWrite  (b'00010000')	    ;Bus address for i2c gripper controller write
    #define LScmd	  (b'00000001')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for lead screw
    #define GTcmd	  (b'00000010')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for gripper tilt
    #define GJcmd	  (b'00000100')	    ;Command to alerting gripper controller
					    ;that next packet is PWM for gripper claw	
    #define screwStop	       (d'94')	    ;PWM stop signal for lead screw	
    #define gripperTiltNeutral (d'88')
	
    #define BANK0	       (h'000')
    #define BANK1	       (h'080')
    #define BANK2	       (h'100')
    #define BANK3	       (h'180')
    				     

    __CONFIG _CONFIG1,    _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC_XT & _FCMEN_OFF & _IESO_OFF

;Context saving variables:
CONTEXT	UDATA_SHR
w_temp		    RES	1	; variable used for context saving
status_temp	    RES	1	; variable used for context saving
pclath_temp	    RES	1
receiveData	    RES	1
transData	    RES	1

;General Variables
GENVAR	UDATA
userMillis	    RES	1
ADRESH0		    RES	1
dly16Ctr	    RES	1	
ADCctr		    RES	1	    
leadScrewPWM	    RES	1	;PWM value to be sent to lead screw motor
gripTiltPWM	    RES	1	;PWM value to be sent to gripper tilt servo
gripTiltPWMc	    RES	1	;Copy of PWM value to be sent to gripper tilt servo	    
gripJawPWM	    RES	1	;PWM value to be sent to gripper claw servo
gripJawPWMc	    RES	1	;Copy of PWM value to be sent to gripper claw servo	    
i2cByteToSend	    RES	1	  
gripperTiltADC	    RES	1	;Copy of gripper tilt ADC value
gripperJawADC	    RES	1	;Copy of gripper Jaw ADC value	    
gripperFlags	    RES	1	;Used for motion control of gripper arm
	    ;b'00000000'
	      ;-------1	    ;Last command was gripper tilt downwards (0=upwards)
	      ;------1-	    ;Last command was gripper tilt neutral (0=not neutral)
	      ;-----1--	    ;Last command was gripper jaw closed (0=open)
	      ;----1---	    ;Last command was gripper jaw neutral (0= not neutral)
				  

;**********************************************************************
    ORG		0x000	
    pagesel		start	; processor reset vector
    goto		start	; go to beginning of program
INT_VECTOR:
    ORG		0x004		; interrupt vector location
INTERRUPT:
    ;SUBROUTINES:	
;variable length mllisecond delay (number of millis needed is already in work register)
delayMillis
    banksel	userMillis
    movwf	userMillis	    ;user defined number of milliseconds
startDly
    banksel	TMR0
    clrf	TMR0
waitTmr0
    banksel	TMR0
    movfw	TMR0
    xorlw	.125	    ;125 * 8uS = 1mS
    btfss	STATUS, Z
    goto	waitTmr0
    banksel	userMillis
    decfsz	userMillis, f   ;reached user defined milliseconds yet?
    goto        startDly
    retlw	0
    
Delay16Us
    banksel	    dly16Ctr
    clrf	    dly16Ctr	;zero out delay counter
begin
    nop			;1 uS (4Mhz clock/4 = 1uS per instruction
    banksel	    dly16Ctr
    incf	    dly16Ctr, f
    movlw	    .16		
    xorwf	    dly16Ctr, w	 ;16 uS passed?
    btfss	    STATUS, Z
    goto	    begin	;no so keep looping
    retlw	    0 
    
Transmit
    movfw	transData	
    banksel	TXREG
    movwf	TXREG		;data to be transmitted loaded into TXREG
				;and then automatically loaded into TSR
    nop
    nop
    banksel	PIR1
wait_trans
    btfss	PIR1, TXIF	;Is TX buffer full? (1=empty, 0=full)
    goto	wait_trans
    retlw	0    
    
;******************************I2C Routines*************************************
;Send START condition and wait for it to complete
I2Cstart
    call	waitMSSP
    banksel	SSPCON2
    bsf		SSPCON2, SEN
    btfsc	SSPCON2, SEN
    goto	$-1
    retlw	0
    
;Send STOP condition and wait for it to complete
I2CStop
    call	waitMSSP
    banksel	SSPCON2
    bsf		SSPCON2, PEN
    btfsc	SSPCON2, PEN	    ;PEN auto cleared by hardware when finished
    goto	$-1
    retlw	0
    
;Send RESTART condition and wait for it to complete
I2Crestart
    call	waitMSSP
    banksel	SSPCON2
    bsf		SSPCON2, RSEN
    retlw	0
    
  ;Send ACK to slave (master is in receive mode)
sendACK
    call	waitMSSP
    banksel	SSPCON2
    bcf		SSPCON2, ACKDT  ;(0=ACK will be sent)
    bsf		SSPCON2, ACKEN	;(ACK is now sent)
    retlw	0    
    
;Send NACK to slave (master is in receive mode)
sendNACK
    call	waitMSSP
    banksel	SSPCON2
    bsf		SSPCON2, ACKDT  ;(1=NACK will be sent)
    bsf		SSPCON2, ACKEN	;(NACK is now sent)
    retlw	0  
    
;I2C wait routine   
waitMSSP
    banksel	SSPSTAT
    btfsc	SSPSTAT, 2	;(1=transmit in progress, 0=no trans in progress
    goto	$-1		;trans in progress so wait
    banksel	SSPCON2
    movfw	SSPCON2		;get copy of SSPCON2
    andlw	b'00011111'	;mask out bits that specify something going on
				;ACEKN, RCEN, PEN, RSEN, SEN = 1 then wait
    btfss	STATUS, Z	;0=all good, proceed
    goto	$-3		;1=not done doing something so retest and wait
    retlw	0    
    
;Send a byte of (command or data) via I2C    
sendI2Cbyte
    banksel	SSPBUF
    movwf	SSPBUF		;byte is already in work register
    ;banksel	SSPSTAT
    ;btfsc	SSPSTAT, 2	;wait till buffer is full (when RW=1=transfer complete)
    ;goto	$-1		;not full, wait here
    call	waitMSSP
    retlw	0
    
;Write to slave device    
I2Csend
    ;Send data and check for error, wait for it to complete
    banksel	i2cByteToSend
    movfw	i2cByteToSend
    call	sendI2Cbyte	    ;load data into buffer
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT    ;ACKSTAT=1 if ACK not received from slave
    goto	$-1	            ;ACK not received
    retlw	0   
    
;Wait for an ACK
waitACK
    banksel	SSPCON2
    btfsc	SSPCON2, ACKSTAT
    goto	$-1
    retlw	0    

start:

     banksel BANK1
    ;************************Configure PORTS************************************
    movlw   b'00101000'		
	     ;----1---		;AN3=lead screw ADC
    movwf   (TRISA ^ BANK1)
    
    movlw   b'11111111'		
	     ;----1---		;PORTC, 3 = SCL
	     ;---1----		;PORTC, 4 = SDA
    movwf   (TRISC ^ BANK1)
    
    movlw   b'00000000'		
    movwf   (TRISD ^ BANK1)
    
    movlw   b'00000011'
	     ;------1-		;PORTE, 1 = gripper claw ADC
	     ;-------1		;PORTE, 0 = gripper tilt ADC
    movwf   (TRISE ^ BANK1)
    
    
    banksel	PORTD
    clrf	PORTD
    clrf	PORTC
    clrf	PORTB
   
;************************Configure timer************************************
    ;With 4Mhz external crystal, FOSC is not divided by 4.
    ;Therefore each instruction is 1/4 of a microsecond (250*10^-9 sec.)
    movlw	b'11000100'	
		 ;1-------	WPUEN=0, all weak pull-ups are disabled
		 ;-1------	INTEDG=1, Interrupt on rising edge of INT pin
		 ;--0-----	TMR0CS=0, TMR0 clock source=internal instruction
			        ;	  (FOSC/4)
		 ;---0----	;TMR0SE=0, disregard
		 ;----0---	;PSA=0, prescaler assigned to TMR0 module
		 ;-----100	;PS<2:0> = 00, TMRO increments once every 32
				;instruction cycles 
				;Every instruction cycle is 250*10^-9 sec (4Mhz), 
				;therefore TMR0 increments once every 32 * 250*10^-9 sec
				;or once every 8uS
    banksel	OPTION_REG	
    movwf	OPTION_REG
    
    ;external crystal:
    movlw	b'00000000'
    banksel	OSCCON
    movwf	OSCCON
    
    ;************************Config ADC:****************************************
    movlw	b'00001000' 
		 ;----1---		;AN3=lead screw
    banksel	ANSELA	    
    movwf	ANSELA
    
    movlw	b'00000011' 
		 ;------1-		;RE1, AN6 = gripper claw
		 ;-------1		;RE0, AN5=gripper tilt
    banksel	ANSELE	    
    movwf	ANSELE
    
    movlw	b'00010000'
		;0-------  ADFM=0 (left justified. 8MSBs are in ADRESH
		;-001----  ADCS<0:2>, bits 4-6 =001. (2.0uS)
			;FOSC/8=4Mhz/8 (doubles instruction cycle time)
			;Instruction Cycle period (TCY) now equals
			;2uS (greater than 1.6uS necessary for ADC)
    banksel	ADCON1
    movwf	ADCON1
    
    banksel	ANSELB
    clrf	ANSELB
    clrf	ANSELD
    
;******************************CONFIGURE UART (for testing):********************
    ;Configure Baud rate
    movlw	b'00111010' ;=58
    banksel	SPBRG
    movwf	SPBRG	    
    
    movlw	b'00000011' ;=3 Total value of SPBRG/n = 826
    banksel	SPBRGH
    movwf	SPBRGH
    
    banksel	BAUDCON
    movlw	b'00001000'
		 ;----1---	BRG16 (16 bit baud rate generator)
    movwf	BAUDCON
    
    ;Enable Transmission:
    movlw	b'00100000'
		 ;-0------  :8-bit transmission (TX9 = 0)
		 ;--1-----  :Enable transmission (TXEN = 1)
		 ;---0----  :Asynchronous mode (SYNC = 0)
		 ;-----0--  :Low speed baud rate (BRGH = 0)
    banksel	TXSTA
    movwf	TXSTA    
    
    ;Enable Reception:
    movlw	b'10010000'
		 ;1-------  :Serial port enabled (SPEN = 1)
		 ;-0------  :8-bit reception (RX9 = 0)
		 ;---1----  :Enable receiver (CREN = 1)
		 ;----0---  :Disable address detection (ADDEN = 0)
    ;			     all bytes are received and 9th bit can be used as
    ;			     parity bit
    banksel	RCSTA
    movwf	RCSTA
    
    ;*********************Configure I2C*****************************************
    movlw	.255	;SCL pin clock period=FOSC/(4*(SSPADD+1))
				;SSPADD=255.
				;Baud=(4*10^6) / (4*(103+1)) = 3900 bps
    banksel	SSPADD
    movwf	SSPADD
    
    movlw	b'10000000'
		 ;1-------	'SMP=1, data sampled at end of data output time
				;slew rate control in 100kHz mode
    banksel	SSPSTAT
    movwf	SSPSTAT
    
    movlw	b'00101000'
		 ;-0------	;SSPOV=receive overflow indicator (read data in 
				;sspbuf before new data comes in to prevent error.
				;Check and clear to ensure sspbuf can be updated
		 ;--1-----	;SSPEN=1 (Enable I2C serial port)
		 ;----1000	;1000 = I2C Master Mode. 
				;Clock=FOSC/(4*(SSPADD+1))
    banksel	SSPCON1
    movwf	SSPCON1
    
    banksel	SSPCON2
    clrf	SSPCON2    
    
    movlw	screwStop
    banksel	leadScrewPWM
    movwf	leadScrewPWM
    movlw	gripperTiltNeutral
    movwf	gripTiltPWM
    movwf	gripJawPWM
    movlw	.129		;Neutral ADC value for gripper servos
    movwf	gripperTiltADC
    movwf	gripperJawADC
    movlw	b'00001010'
    movwf	gripperFlags	;Load a gripper state of neutral position for
				;tilt and jaws position
    
  
;***************************MAIN************************************************
mainLoop
;**********************Lead Screw***********************************************
    ;Set AN3 as analog input for AD conversion and start AD conversion
    movlw	b'00001101'
		 ;-00011	CHS<0:4> (bits 2-6) = 00011 = pin AN3/PORTA, 3 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitLeadScrew
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitLeadScrew
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in lead screw joystick 
    ;Neutral ADC value range = 127-131
    movlw	.127
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoScrewSlop
    movlw	.129
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	NoScrewSlop
    ;Joystick is in neutral position
    movlw	screwStop
    banksel	leadScrewPWM
    movwf	leadScrewPWM
    goto	doneLeadScrew
    
NoScrewSlop
    ;Determine if we are moving forward or reverse:
    movlw	.129	    ;129=ADC neutral value
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C   ;C=0 for neg result
    goto	ScrewRev
    
ScrewFwd    ;We are extending arm outwards
    movlw	.95	    
    banksel	leadScrewPWM
    movwf	leadScrewPWM	;Start with minimum PWM forward speed
    movlw	.131
    movwf	ADCctr		;Start with minimum fwd ADC value
calcFwdScrew	;Start calculating our fwd screw speed
    movlw	.118		;Make sure we havent exceeded max PWM value 
    banksel	leadScrewPWM
    subwf	leadScrewPWM, w
    btfsc	STATUS, C	;C=0if neg result
    goto	doneLeadScrew	;We have hit a PWM value of 118 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	doneLeadScrew
    incf	leadScrewPWM, f	;Increment lead screw PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcFwdScrew	;Keep calculating
    
ScrewRev    ;We are bringing arm inwards
    movlw	.68	    
    banksel	leadScrewPWM
    movwf	leadScrewPWM	;Start with maximum PWM reverse speed
    movlw	.99
    movwf	ADCctr		;Start with maximum rev ADC value
calcRevScrew	;Start calculating our rev screw speed 
    movlw	.93		;Make sure we havent exceeded rev PWM value
    banksel	leadScrewPWM
    subwf	leadScrewPWM, w
    btfsc	STATUS, C	;C=0if neg result
    goto	doneLeadScrew	;We have hit a PWM value of 93 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	doneLeadScrew
    incf	leadScrewPWM, f	;Increment lead screw PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcRevScrew	;Keep calculating
    
doneLeadScrew	;Send lead screw value to controller
    
    
;*************************Gripper Tilt******************************************  
GripTilt
;Set AN5 as analog input for AD conversion and start AD conversion
    movlw	b'00010101'
		 ;-00101	CHS<0:4> (bits 2-6) = 00101 = pin AN5/PORTE, 0 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitGripperTilt
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitGripperTilt
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in gripper tilt joystick 
    ;Neutral ADC value range = 127-131
    movlw	.125
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoTiltSlop
    movlw	.132
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	NoTiltSlop
    ;Joystick is in neutral position
    ;movlw	gripperTiltNeutral
    ;banksel	gripTiltPWM	    ;Load neutral PWM signal
    ;movwf	gripTiltPWM
    ;movlw	.129
    ;movwf	gripperTiltADC	    ;Save neutral ADC value
    bsf		gripperFlags, 1	    ;Save state of gripper neutral position
    goto	doneGripTilt
    
NoTiltSlop
    ;Determine if we are tilting down or up:
    movlw	.129	    ;129=ADC neutral value
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C   ;C=0 for neg result
    goto	TILTUP
    
TILTDOWN    ;We are tilting the gripper downwards
    ;Determine if this particular ADC is due to the joystick returning to a neutral 
    ;position after being tilted downwards. We want the gripper to remain tilted
    ;in the position the user placed it in and any ADC performed as the jostick
    ;springs back to neutral messes that up.
    banksel	gripperFlags
    btfsc	gripperFlags, 0	    ;Was last command a tilt down or up?
    goto	CkTiltDownHold	    ;It was tilt down
    goto	GetTiltDown	    ;It was tilt up
CkTiltDownHold	;Last command was tilt downwards
    ;banksel	gripperFlags
    ;btfsc	gripperFlags, 1	    ;Was the gripper tilt in a neutral position
				    ;prior to this? (1=neutral)
    ;goto	GetTiltDown	    ;Yes so get the PWM value.
	;Make sure the new ADC value is not in between the current ADC value and
	;a neutral value. If so, it is due to the joystick springing back to neutral
    banksel	ADRESH0
    movfw	ADRESH0
    subwf	gripperTiltADC, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	doneGripTilt	;ADC is due to joystick springing back to neutral
				;so disregard it.
GetTiltDown    
    ;Get the PWM value
    movlw	.98	    
    banksel	gripTiltPWMc
    movwf	gripTiltPWMc	;Start with minimum PWM tilt-down value
    movlw	.131
    movwf	ADCctr		;Start with minimum ADC tilt-down value
calcDownTilt	;Start calculating our downward tilt
    movlw	.131		;Make sure we havent exceeded max PWM value (131)
    banksel	gripTiltPWMc
    subwf	gripTiltPWMc, w
    btfsc	STATUS, C	;C=0if neg result
    goto	DoneDownCalc	;We have hit a PWM value of 131 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneDownCalc
    incf	gripTiltPWMc, f	;Increment gripper tilt PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcDownTilt	;Keep calculating 
DoneDownCalc
    ;Now combine the current PWM calculation with the old PWM value
    ;Determine if old position was in upper or lower tilt range
    movlw	.94
    subwf	gripTiltPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	OldPositionLower
OldPostionUpper
    ;Get displacement of new value from neutral PWM value
    movlw	.94
    subwf	gripTiltPWMc, w
    ;Now add this to the old PWM value
    addwf	gripTiltPWM, f
    goto	DoneAdjustingDown
OldPositionLower    
    ;Get difference between old and new PWM values (New value is greater)
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    subwf	gripTiltPWMc, w
    ;Now add this difference to the old PWM value
    addwf	gripTiltPWM, f
    
    ;Make sure we haven't exceeded max value of 131
    movlw	.132
    subwf	gripTiltPWM, w	;Should get a neg result if max hasn't been exceeded
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneAdjustingDown   ;Value is ok so proceed to exit
    movlw	.131		
    movwf	gripTiltPWM	;Max was exceeded so load max PWM value
DoneAdjustingDown    
    banksel	ADRESH0
    movfw	ADRESH0
    movwf	gripperTiltADC	;Save gripper tilt ADC value
    bsf		gripperFlags, 0	;Set flag for last command being tilt down
    bcf		gripperFlags, 1	;Set flag for last command placing gripper tilt
				;in a non-neutral position.
    goto	doneGripTilt				
    
TILTUP	    ;We are tilting the gripper upwards
    ;Determine if this particular ADC is due to the joystick returning to a neutral 
    ;position after being tilted upwards. We want the gripper to remain tilted
    ;in the position the user placed it in and any ADC performed as the joystick
    ;springs back to neutral messes that up.
    banksel	gripperFlags
    btfsc	gripperFlags, 0	    ;Was last command a tilt down or up?
    goto	GetTiltUp	    ;It was tilt down
    goto	CkTiltUpHold	    ;It was tilt up
    
CkTiltUpHold
    ;banksel	gripperFlags
    ;btfsc	gripperFlags, 1	    ;Was the gripper tilt in a neutral position
				    ;prior to this? (1=neutral)
    ;goto	GetTiltUp	    ;Yes so get the PWM value.
	;Make sure the new ADC value is not in between the current ADC value and
	;a neutral value. If so, it is due to the joystick springing back to neutral
    banksel	gripperTiltADC
    movfw	gripperTiltADC
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	doneGripTilt	;ADC is due to joystick springing back to neutral
				;so disregard it.
GetTiltUp
    movlw	.56	    
    banksel	gripTiltPWMc
    movwf	gripTiltPWMc	;Start with minimum PWM tilt-up value
    movlw	.99
    movwf	ADCctr		;Start with minimum ADC tilt-up value
calcUpTilt	;Start calculating our upward tilt 
    movlw	.91		;Make sure we havent exceeded down PWM value 
    banksel	gripTiltPWMc
    subwf	gripTiltPWMc, w
    btfsc	STATUS, C	;C=0if neg result
    goto	DoneUpCalc	;We have hit a PWM value of 90 so exit loop   
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneUpCalc
    incf	gripTiltPWMc, f	;Increment gripper tilt PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcUpTilt	;Keep calculating
DoneUpCalc    
    ;Now combine the current PWM calculation with the old PWM value
    ;Determine if old position was in upper or lower tilt range
    movlw	.94
    subwf	gripTiltPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	OldPWMlower
OldPWMupper
    ;Get difference between old and new PWM values (New value is lower)
    banksel	gripTiltPWMc
    movfw	gripTiltPWMc
    subwf	gripTiltPWM, w
    ;Now subtract this difference to the old PWM value
    subwf	gripTiltPWM, f
    goto	DoneAdjustingUp
OldPWMlower
    ;Get displacement of new value from neutral PWM value
    movlw	.94
    banksel	gripperTiltADC	;We are just using this as a dummy variable for 
				;For math purposes
    movwf	gripperTiltADC
    movfw	gripTiltPWMc
    subwf	gripperTiltADC, w
    ;Now subtract this to the old PWM value
    subwf	gripTiltPWM, f
DoneAdjustingUp
    banksel	ADRESH0
    movfw	ADRESH0
    movwf	gripperTiltADC	;Save gripper tilt ADC value
    bcf		gripperFlags, 0	;Set flag for last command being tilt up
    bcf		gripperFlags, 1	;Set flag for last command placing gripper tilt
				;in a non-neutral position.
				
doneGripTilt    ;Send grip tilt value to controller
    
;**************************Gripper Claw*****************************************
GripJaw    
    ;Set AN6 as analog input for AD conversion and start AD conversion
    movlw	b'00011001'
		 ;-00110	CHS<0:4> (bits 2-6) = 00110 = pin AN6/PORTE, 1 as analog input
		;------0-   	Stop AD conversion
		;-------1	Enable ADC
    banksel	ADCON0
    movwf	ADCON0
    
    call	Delay16Us	    ;Wait 16uS acquisition time
    
    banksel	ADCON0
    bsf		ADCON0, GO	    ;Start AD conversion
waitGripperJaw
    btfsc	ADCON0, NOT_DONE    ;Wait until AD conversion completes
    goto	waitGripperJaw
    banksel	ADRESH
    movfw	ADRESH
    banksel	ADRESH0
    movwf	ADRESH0
    
    ;Check for slop in gripper claw joystick 
    ;Neutral ADC value range = 127-131
    movlw	.126
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	NoJawSlop
    movlw	.132
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	NoJawSlop
    ;Joystick is in neutral position
    ;movlw	.94
    ;banksel	gripJawPWM
    ;movwf	gripJawPWM
    ;goto	GripperDone
    bsf		gripperFlags, 3	    ;Save state of jaws to neutral position
    goto	GripperDone
    
NoJawSlop  
    ;Determine if we are opening or closing gripper jaws
    movlw	.129	    ;129=ADC neutral value
    banksel	ADRESH0
    subwf	ADRESH0, w
    btfss	STATUS, C   ;C=0 for neg result
    goto	OPENJAWS
CLOSEJAWS    ;We are closing jaws
    ;Determine if this particular ADC is due to the joystick returning to a neutral 
    ;position after being moved right. We want the jaws to remain in the
    ;position the user placed it in and any ADC performed as the joystick
    ;springs back to neutral messes that up.
    banksel	gripperFlags
    btfsc	gripperFlags, 2	    ;Was last command open or close?
    goto	CkCloseHold	    ;It was close
    goto	GetClosed	    ;It was open
CkCloseHold ;Last command was to close jaws
	;Make sure the new ADC value is not in between the current ADC value and
	;a neutral value. If so, it is due to the joystick springing back to neutral
    banksel	ADRESH0
    movfw	ADRESH0
    subwf	gripperJawADC, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	GripperDone	;ADC is due to joystick springing back to neutral
				;so disregard it.
GetClosed    
    ;Get the PWM value				
    movlw	.98	    
    banksel	gripJawPWMc
    movwf	gripJawPWMc	;Start with minimum PWM jaws-close value
    movlw	.131
    movwf	ADCctr		;Start with minimum ADC jaws-close value
calcJawsClosed	;Start calculating our closed jaws
    movlw	.131		;Make sure we havent exceeded max closed PWM value 
    banksel	gripJawPWMc
    subwf	gripJawPWMc, w
    btfsc	STATUS, C	;C=0if neg result
    goto	DoneClosedCalc	;We have hit a PWM value of 131 so exit loop
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneClosedCalc
    incf	gripJawPWMc, f	;Increment gripper jaws PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcJawsClosed	;Keep calculating     
DoneClosedCalc
    ;Now combine the current PWM calculation with the old PWM value
    ;Determine if old position was in upper or lower half of the range
    movlw	.94
    subwf	gripJawPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	OldJawsLower
OldJawsUpper
    ;Get displacement of new value from neutral PWM value
    movlw	.94
    subwf	gripJawPWMc, w
    ;Now add this to the old PWM value
    addwf	gripJawPWM, f
    goto	DoneAdjustingClosed    
OldJawsLower    
    ;Get difference between old and new PWM values (New value is greater)
    banksel	gripJawPWM
    movfw	gripJawPWM
    subwf	gripJawPWMc, w
    ;Now add this difference to the old PWM value
    addwf	gripJawPWM, f
    ;Make sure we haven't exceeded max value of 131
    movlw	.132
    subwf	gripJawPWM, w	;Should get a neg result if max hasn't been exceeded
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneAdjustingClosed   ;Value is ok so proceed to exit
    movlw	.131		
    movwf	gripJawPWM	;Max was exceeded so load max PWM value
DoneAdjustingClosed    
    banksel	ADRESH0
    movfw	ADRESH0
    movwf	gripperJawADC	;Save gripper tilt ADC value
    bsf		gripperFlags, 2	;Set flag for last command being to close jaws
    bcf		gripperFlags, 3	;Set flag for last command placing gripper tilt
				;in a non-neutral position.
    goto	GripperDone
    
OPENJAWS    ;We are opening jaws
    ;Determine if this particular ADC is due to the joystick returning to a neutral 
    ;position after being moved left. We want the jaws to remain in the 
    ;position the user placed them in and any ADC performed as the joystick
    ;springs back to neutral messes that up.
    banksel	gripperFlags
    btfsc	gripperFlags, 2	    ;Was last command jaws open or closed?
    goto	GetOpen		    ;It was close
    goto	CkOpenHold	    ;It was open
CkOpenHold
	;Make sure the new ADC value is not in between the current ADC value and
	;a neutral value. If so, it is due to the joystick springing back to neutral
    banksel	gripperJawADC
    movfw	gripperJawADC
    subwf	ADRESH0, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	GripperDone	;ADC is due to joystick springing back to neutral
				;so disregard it.
GetOpen    
    movlw	.56	    
    banksel	gripJawPWMc
    movwf	gripJawPWMc	;Start with maximum PWM jaws-open value
    movlw	.99
    movwf	ADCctr		;Start with maximum ADC jaws-open value
calcJawsOpen	;Start calculating our opened jaws 
    movlw	.91		;Make sure we havent exceeded max opened PWM value 
    banksel	gripJawPWMc
    subwf	gripJawPWMc, w
    btfsc	STATUS, C	;C=0if neg result
    goto	DoneOpenCalc	;We have hit a PWM value of 90 so exit loop   
    
    movfw	ADCctr
    subwf	ADRESH0, w
    btfss	STATUS, C	;C=0 if neg result
    goto	DoneOpenCalc
    incf	gripJawPWMc, f	;Increment gripper jaws PWM value by one
    movlw	.1
    addwf	ADCctr		;Add 1 to ADCctr
    goto	calcJawsOpen	;Keep calculating    
DoneOpenCalc    
    ;Now combine the current PWM calculation with the old PWM value
    ;Determine if old position was in upper or lower range
    movlw	.94
    subwf	gripJawPWM, w
    btfsc	STATUS, C	;C=0 if neg result
    goto	OldJlower
OldJUpper
    ;Get difference between old and new PWM values (New value is lower)
    banksel	gripJawPWMc
    movfw	gripJawPWMc
    subwf	gripJawPWM, w
    ;Now subtract this difference to the old PWM value
    subwf	gripJawPWM, f
    goto	DoneAdjustingOpen
OldJlower    
    ;Get displacement of new value from neutral PWM value
    movlw	.94
    banksel	gripperJawADC	;We are just using this as a dummy variable for 
				;For math purposes
    movwf	gripperJawADC
    movfw	gripJawPWMc
    subwf	gripperJawADC, w
    ;Now subtract this to the old PWM value
    subwf	gripJawPWM, f				
DoneAdjustingOpen    
    banksel	ADRESH0
    movfw	ADRESH0
    movwf	gripperJawADC	;Save gripper jaw ADC value
    bcf		gripperFlags, 2	;Set flag for last command being open jaws
    bcf		gripperFlags, 3	;Set flag for last command placing gripper jaws
				;in a non-neutral position.
GripperDone  ;Send grip jaw value to controller
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for the lead screw ESC
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	LScmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send lead screw PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	leadScrewPWM
    movfw	leadScrewPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for gripper tilt
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	GTcmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    ;Send gripper tilt PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	gripTiltPWM
    movfw	gripTiltPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    
    ;Send command letting controller know that next packet is a PWM value 
    ;for gripper jaw
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    movlw	GJcmd
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
    ;Send gripper jaw PWM value
    pagesel	I2Cstart
    call	I2Cstart
    pagesel$
    movlw	gripperWrite
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    banksel	gripJawPWM
    movfw	gripJawPWM
    banksel	i2cByteToSend
    movwf	i2cByteToSend
    pagesel	I2Csend
    call	I2Csend
    pagesel$
    pagesel	waitACK
    call	waitACK
    pagesel$
    pagesel	I2CStop
    call	I2CStop
    pagesel$
  
    
    ;movlw	.30
    ;call	delayMillis
    
    ;UART test for pwm values
    ;banksel	leadScrewPWM
    ;movfw	leadScrewPWM
    ;banksel	transData
    ;movwf	transData
    ;call	Transmit
    ;movlw	.5
    ;call	delayMillis
    
    ;banksel	gripTiltPWM
    ;movfw	gripTiltPWM
    ;banksel	transData
    ;movwf	transData
    ;call	Transmit
    ;movlw	.5
    ;call	delayMillis
    
    ;banksel	gripJawPWM
    ;movfw	gripJawPWM
    ;banksel	transData
    ;movwf	transData
    ;call	Transmit
    ;movlw	.5
    ;call	delayMillis
    
    ;movlw	.30
    ;call	delayMillis
    
    goto	mainLoop
    
    END                       
























